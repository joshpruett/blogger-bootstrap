Blogger Bootstrap
============

A Bootstrap template for Classic Blogger.

### Features ###

  * CDN Bootstrap
  * CDN JQuery
  * CDN PrettyPrint
  * Search Bar
  * Archives Widget
  * Feeds Widget